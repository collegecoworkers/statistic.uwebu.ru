<!DOCTYPE html>
<html>
<head>
	<!-- Кодировка страницы -->
	<meta charset="UTF-8">
	
	<!-- Название сайта -->
	<title>Сбор информации из соц сетей</title>
	
	<link href="assets/css/application.min.css" rel="stylesheet">
	<link rel="shortcut icon" href="assets/img/favicon.png">
</head>
<body class="background-dark">
	<div class="logo">
		<h4><a href="/">Сбор <strong>Инфы</strong></a></h4>
	</div>

	<?php include('widgets/sidebar.php') ?>
	<div class="wrap">
		<div class="content container">
			<?php include('widgets/menu.php') ?>
			<?php
				if (isset($page)) {
					$page = __DIR__."/page.".strtolower($page).".php";
					if (file_exists($page)) {
						include($page);
					} else { return; }
				} else {
					echo "<h1>Шаблон страницы не найден!</h1>";
				}
			?>
		</div>
		<div class="loader-wrap hiding hide">
			<i class="fa fa-spinner fa-spin"></i>
		</div>
		<div class="loader-wrap hiding hide">
			<i class="fa fa-spinner fa-spin"></i>
		</div>
	</div>

	<?php include('widgets/footer.php') ?>
</body>
</html>
