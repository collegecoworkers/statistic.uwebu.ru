<div class="row">
	<div class="col-md-12">
		<h2 class="page-title"><?= $title ?> <small></small></h2>
	</div>
</div>
<div class="row">
	<div class="col-md-10">
		<section class="widget">
			<header>
				<h4>
					<i class="fa fa-list-alt"></i>
					Список поиска
				</h4>
			</header>
			<div class="body">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>#</th>
							<th>uid</th>
							<th class="hidden-xs-portrait">Имя</th>
							<th>Действия</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($users as $item): ?>
							<tr>
								<td><?= $item['id'] ?></td>
								<td><?= $item['uid'] ?></td>
								<td><?= $item['data']->first_name . ' ' . $item['data']->last_name ?></td>
								<td><a href="/show.php?id=<?= $item['uid'] ?>">Посмотреть</a></td>
							</tr>
						<?php endforeach ?>
					</tbody>
				</table>
			</div>
		</section>
	</div>
</div>
