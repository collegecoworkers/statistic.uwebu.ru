<div class="row">
	<div class="col-md-12">
		<h2 class="page-title"><?= $title ?> <small></small></h2>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<section class="widget">
			<header>
				<h4><i class="fa fa-cogs"></i> Заполните форму и получите информацию</h4>
			</header>
			<div class="body">
				<form class="form-horizontal" method="post">
					<fieldset>
						<div class="control-group">
							<label class="control-label" for="id-purpose">ID пользователя</label>
							<div class="controls form-group">
								<div class="col-sm-8">
									<input type="text" id="id-purpose" name="id-purpose" class="form-control" placeholder="id1" required="">
								</div>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Поля</label>
							<div class="controls form-group">
								<?php foreach ($fields as $item): ?>
									<label class="checkbox"><input type="checkbox" checked="checked" name="<?= $item['name']  ?>" class="iCheck"><?= $item['label']  ?></label>
								<?php endforeach ?>
							</div>
						</div>
					</fieldset>
					<div class="form-actions">
						<div>
							<button type="submit" class="btn btn-primary">Отправить</button>
						</div>
					</div>
				</form>
			</div>
		</section>
	</div>
</div>
