<div class="row">
	<div class="col-md-12">
		<h2 class="page-title"><?= $title ?> <small></small></h2>
	</div>
</div>
<div class="row">
		<div class="col-md-7">
				<section class="widget">
						<header>
								<h4><i class="fa fa-user"></i> Пользователь</h4>
						</header>
						<div class="body">
								<form id="user-form" class="form-horizontal label-left" novalidate="novalidate" method="post" data-validate="parsley">
										<div class="row">
												<div class="col-md-4">
														<div class="text-align-center">
																<img class="img-circle" src="<?= $user->photo_100 ?>" alt="64x64" style="height: 112px;">
														</div>
												</div>
												<div class="col-md-8">
														<h3 class="no-margin"><?= $user->first_name . ' ' . $user->last_name?></h3>
														<address>
																<p>uid: <?= $user->uid ?><p>
																<p>пол: <?= $user->sex ?><p>
																<p>domain: <?= $user->domain ?><p>
														</address>
												</div>
										</div>
										<fieldset>
												<legend>Информация об аккаунте</legend>
										</fieldset>
										<fieldset>
											<style>
												.w-l{width: 45% !important}
												.p-v{padding-top:5px !important}
											</style>
												<?php foreach ($fields as $item): ?>
													<div class="control-group">
															<label class="control-label w-l"><?= $item['label'] ?></label>
															<div class="controls form-group p-v"><p><?= $item['val'] ?></p></div>
													</div>
												<?php endforeach ?>
										</fieldset>
										<!-- <div class="form-actions">
												<button type="button" class="btn btn-default">Update</button>
										</div> -->
								</form>
						</div>
				</section>
		</div>
</div>
