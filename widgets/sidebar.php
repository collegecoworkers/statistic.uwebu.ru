<nav id="sidebar" class="sidebar nav-collapse collapse">
    <ul id="side-nav" class="side-nav">
        <li class="<?= $alias == 'home' ? 'active' : ''?>">
            <a href="/"><i class="fa fa-home"></i> <span class="name">Главная</span></a>
        </li>
        <li class="<?= $alias == 'all-users' ? 'active' : ''?>">
            <a href="/all-users.php"><i class="fa fa-bar-chart-o"></i> <span class="name">История поиска</span></a>
        </li>
    </ul>
    <div id="sidebar-settings" class="settings">
        <button type="button"
                data-value="icons"
                class="btn-icons btn btn-transparent btn-sm">Icons</button>
        <button type="button"
                data-value="auto"
                class="btn-auto btn btn-transparent btn-sm">Auto</button>
    </div>
</nav>