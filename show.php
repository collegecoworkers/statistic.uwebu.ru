<?php

	/*
	*  Вывод данных о пользователе из бд
	*/

	// подключение основного файла приложения
	require("core/app.php");

	if(!isset($_GET['id'])){
		redirect_to('/');
	}

	$uid = $_GET['id'];

	// получение пользователя из бд
	$user = $db->getItem([
		'item' => 'purpose',
		'whr' => ['uid', $uid]
	]);

	if($user == null){
		redirect_to('/');
	}

	$page['alias'] = 'show';
	$page['title'] = 'Просмотр';

	$page['user'] = json_decode($user['data']);

	$page['fields'] = [];
 
	$page['fields'][] = ['label' => 'Cтраница верифицирована:','val' => $page['user']->verified ? 'Да' : 'Нет' ];
	if(isset($page['user']->relation) and $page['user']->relation != '') $page['fields'][] = ['label' => 'Cемейное положение:','val' => $page['user']->relation ];
	if(isset($page['user']->about) and $page['user']->about != '') $page['fields'][] = ['label' => 'Cодержимое поля «О себе» из профиля.','val' => $page['user']->about];
	if(isset($page['user']->bdate) and $page['user']->bdate != '') $page['fields'][] = ['label' => 'Дата рождения','val' => $page['user']->bdate];
	if(isset($page['user']->mobile_phone) and $page['user']->mobile_phone != '') $page['fields'][] = ['label' => 'Номер мобильного','val' => $page['user']->mobile_phone];
	if(isset($page['user']->home_phone) and $page['user']->home_phone != '') $page['fields'][] = ['label' => 'Дополнительный номер','val' => $page['user']->home_phone];

	if(isset($page['user']->counters) and $page['user']->counters != ''){
		if(isset($page['user']->counters->albums) and $page['user']->counters->albums != '') $page['fields'][] = ['label' => 'Количество фотоальбомов','val' => $page['user']->counters->albums];
		if(isset($page['user']->counters->videos) and $page['user']->counters->videos != '') $page['fields'][] = ['label' => 'Количество видеозаписей','val' => $page['user']->counters->videos];
		if(isset($page['user']->counters->audios) and $page['user']->counters->audios != '') $page['fields'][] = ['label' => 'Количество аудиозаписей','val' => $page['user']->counters->audios];
		if(isset($page['user']->counters->photos) and $page['user']->counters->photos != '') $page['fields'][] = ['label' => 'Количество фотографий','val' => $page['user']->counters->photos];
		if(isset($page['user']->counters->notes) and $page['user']->counters->notes != '') $page['fields'][] = ['label' => 'Количество заметок','val' => $page['user']->counters->notes];
		if(isset($page['user']->counters->friends) and $page['user']->counters->friends != '') $page['fields'][] = ['label' => 'Количество друзей','val' => $page['user']->counters->friends];
		if(isset($page['user']->counters->groups) and $page['user']->counters->groups != '') $page['fields'][] = ['label' => 'Количество сообществ','val' => $page['user']->counters->groups];
		if(isset($page['user']->counters->online_friends) and $page['user']->counters->online_friends != '') $page['fields'][] = ['label' => 'Количество друзей онлайн','val' => $page['user']->counters->online_friends];
		if(isset($page['user']->counters->mutual_friends) and $page['user']->counters->mutual_friends != '') $page['fields'][] = ['label' => 'Количество общих друзей','val' => $page['user']->counters->mutual_friends];
		if(isset($page['user']->counters->user_videos) and $page['user']->counters->user_videos != '') $page['fields'][] = ['label' => 'Количество видеозаписей с пользователем','val' => $page['user']->counters->user_videos];
		if(isset($page['user']->counters->followers) and $page['user']->counters->followers != '') $page['fields'][] = ['label' => 'Количество подписчиков','val' => $page['user']->counters->followers];
		if(isset($page['user']->counters->pages) and $page['user']->counters->pages != '') $page['fields'][] = ['label' => 'Количество объектов в блоке «Интересные страницы»','val' => $page['user']->counters->pages];
	}

	if(isset($page['user']->personal) and $page['user']->personal != ''){
		if(isset($page['user']->personal->political) and $page['user']->personal->political != '') $page['fields'][] = ['label' => 'Политические предпочтения','val' => $page['user']->personal->political];
		if(isset($page['user']->personal->people_main) and $page['user']->personal->people_main != '') $page['fields'][] = ['label' => 'Главное в людях','val' => $page['user']->personal->people_main];
		if(isset($page['user']->personal->life_main) and $page['user']->personal->life_main != '') $page['fields'][] = ['label' => 'Главное в жизни','val' => $page['user']->personal->life_main];
		if(isset($page['user']->personal->smoking) and $page['user']->personal->smoking != '') $page['fields'][] = ['label' => 'Отношение к курению','val' => $page['user']->personal->smoking];
		if(isset($page['user']->personal->alcohol) and $page['user']->personal->alcohol != '') $page['fields'][] = ['label' => 'Отношение к алкоголю','val' => $page['user']->personal->alcohol];
	}
	if(isset($page['user']->quotes) and $page['user']->quotes != '') $page['fields'][] = ['label' => 'любимые цитаты.','val' => nl2br($page['user']->quotes)];

	// echo "<pre>"; print_r($page['user']); die;

	renderPage('show', $page);

?>