<?php

	/*
	*  Главная страница
	*/

	// подключение основного файла приложения
	require("core/app.php");

	// 
	$page['fields'] = [
		['label' => 'страница пользователя верифицирована','name' => 'verified'],
		['label' => 'содержимое поля «О себе» из профиля.','name' => 'about'],
		['label' => 'дата рождения','name' => 'bdate'],
		['label' => 'информация о телефонных номерах','name' => 'contacts'],
		['label' => 'количество различных объектов у пользователя','name' => 'counters'],
		['label' => 'короткий адрес страницы','name' => 'domain'],
		['label' => 'внешние сервисы, в которые настроен экспорт из ВК ','name' => 'exports'],
		['label' => 'время последнего посещения','name' => 'last_seen'],
		['label' => 'семейное положение','name' => 'relation'],
		['label' => 'информация о полях из раздела «Жизненная позиция».','name' => 'personal'],
		['label' => 'любимые цитаты.','name' => 'quotes'],
		['label' => 'пол','name' => 'sex'],
	];

	// получение формы
	if (isset($_POST['id-purpose'])) {

		// проверка существует ли пользователь
		$uid = $_POST['id-purpose'];
		$user = $db->getItem([
			'item' => 'purpose',
			'whr' => ['uid', $uid]
		]);
		$fs = ['photo_100'];
		foreach ($page['fields'] as $item) {
			if(isset($_POST[$item['name']]))
				$fs[] = $item['name'];
		}

		// получение данных из вк
		$result = AddInfo::getDataUserFromVK($uid, $fs);

		if($result != null){
			$result = json_encode($result, JSON_UNESCAPED_UNICODE);
			if($user != null){
				// обновление данных о пользователе
				$db->editItem([
					'item' => 'purpose',
					'whr' => ['uid' => $uid],
					'vals' => [
						'data' => $result,
					]
				]);
				// перенаправление на страницу просмотра
				redirect_to('show.php?id=' . $uid);
			} else {
				// добавление данных о пользователе
				if($db->addItem([
					'item' => 'purpose',
					'vals' => [
						'uid' => $uid,
						'data' => $result,
					]
				]))
					// перенаправление на страницу просмотра
					redirect_to('show.php?id=' . $uid);
				else
					die('Error');
			}
		}

	}

	$page['alias'] = 'home';
	$page['title'] = 'Главная';

	renderPage('home', $page);

?>