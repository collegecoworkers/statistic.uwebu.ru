<?php

class AddInfo {

	// функция возвращающая данные о пользователе,
	// принимает на вход: id пользователя; необходиные поля, по которым нужно получить инфу
	public static function getDataUserFromVK($user, $fields, $token = null){

		// время последнего посещения
		$last_seen_platform = [
			'1' => 'мобильная версия',
			'2' => 'приложение для iPhone',
			'3' => 'приложение для iPad',
			'4' => 'приложение для Android',
			'5' => 'приложение для Windows Phone',
			'6' => 'приложение для Windows 10',
			'7' => 'полная версия сайта',
			'8' => 'VK Mobile',
		];

		// Жизненная позиция
		$personal = [
			'people_main' => [
				'1' => 'ум и креативность',
				'2' => 'доброта и честность',
				'3' => 'красота и здоровье',
				'4' => 'власть и богатство',
				'5' => 'смелость и упорство',
				'6' => 'юмор и жизнелюбие',
			],
			'life_main' => [
				'1' => 'семья и дети',
				'2' => 'карьера и деньги',
				'3' => 'развлечения и отдых',
				'4' => 'наука и исследования',
				'5' => 'совершенствование мира',
				'6' => 'саморазвитие',
				'7' => 'красота и искусство',
				'8' => 'слава и влияние',
			],
			'smoking' => [
				'1' => 'резко негативное',
				'2' => 'негативное',
				'3' => 'компромиссное',
				'4' => 'нейтральное',
				'5' => 'положительное',
			],
			'alcohol' => [
				'1' => 'резко негативное',
				'2' => 'негативное',
				'3' => 'компромиссное',
				'4' => 'нейтральное',
				'5' => 'положительное',
			],
			'political' => [
				'1' => 'коммунистические',
				'2' => 'социалистические',
				'3' => 'умеренные',
				'4' => 'либеральные',
				'5' => 'консервативные',
				'6' => 'монархические',
				'7' => 'ультраконсервативные',
				'8' => 'индифферентные',
				'9' => 'либертарианские',
			]
		];

		// семейное положение
		$relation = [
			'1' => 'не женат/не замужем',
			'2' => 'есть друг/есть подруга',
			'3' => 'помолвлен/помолвлена',
			'4' => 'женат/замужем',
			'5' => 'всё сложно',
			'6' => 'в активном поиске',
			'7' => 'влюблён/влюблена',
			'8' => 'в гражданском браке',
			'0' => 'не указано.',
		];

		// пол
		$sex = [
			'1' => 'женский',
			'2' => 'мужской',
			'0' => 'пол не указан',
		];

		// параметры для запроса
		$request_params = [
			'user_ids' => $user,
			'fields' => implode(',', $fields),
		];

		// добалвение токена если есть
		if($token != null){
			$request_params['access_token'] = $token;
		}
		// формирование запроса
		$url = 'https://api.vk.com/method/users.get?' . http_build_query($request_params);

		// оптравка запроса
		$results = file_get_contents($url);
		// декодирование ответа
		$results = json_decode($results);

		// если есть ошибки
		if(isset($results->error)){
			return null;
		}

		$data = $results->response[0];

		// изменеение некоторых числовых данных на строковое значение
		if(isset($data->last_seen->platform)) $data->last_seen->platform = $last_seen_platform[$data->last_seen->platform];
		if(isset($data->personal->people_main)) $data->personal->people_main = $personal['people_main'][$data->personal->people_main];
		if(isset($data->personal->political)) $data->personal->political = $personal['political'][$data->personal->political];
		if(isset($data->personal->life_main)) $data->personal->life_main = $personal['life_main'][$data->personal->life_main];
		if(isset($data->personal->smoking)) $data->personal->smoking = $personal['smoking'][$data->personal->smoking];
		if(isset($data->personal->alcohol)) $data->personal->alcohol = $personal['alcohol'][$data->personal->alcohol];
		if(isset($data->relation)) $data->relation = $relation[$data->relation];
		if(isset($data->sex)) $data->sex = $sex[$data->sex];

		return $data;
	}
}
?>
