<?php

function redirect_to($new_location = '/'){
	header("Location: ". $new_location);
	exit; 
}

function add_to_uri($key = '', $val = '', $uri = NULL){
	
	if($uri == NULL)
		$uri = $_SERVER['REQUEST_URI'];

	if($key == '') return $uri;
	if(!isset($_GET[$key])){
		return $uri . ((strpos($uri, '?') !== false) ? '&' : '?' ). $key . '=' . $val;
	} else {
		$qs = $_SERVER['QUERY_STRING'];

		preg_match('~'.$key.'\=.+~', $qs, $matches);
		$uri = str_replace($matches[0], $key . '=' . $val , $qs);

		return '?' . $uri;
	}
}
