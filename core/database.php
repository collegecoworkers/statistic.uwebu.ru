<?php

function dbg($p){
	if(is_string($p)){
		die($p);
	} else {
		echo "<pre>";
		print_r($p);
		die();
	}
}

// Класс для простой работы с базой данных MySQL
class Database {

	// переменная для хранения соединения MySQLi
	public $connection;
	// временная переменная для хранения настроек соединения
	private $config;

	private $join_type;

	// функция создания нового объекта БД
	public function __construct() {
		// загрузка файла конфигурации
		$this->load_config();

		// создание нового подключения с параметрами
		$this->connection = new mysqli(
			$this->config['host'],
			$this->config['user'],
			$this->config['password'],
			$this->config['db_name']
			);

		// установка кодировки данных в UTF8
		$this->connection->query('set names utf8');

		$this->join_type = [
		'inner' => 'INNER',
		'left' => 'LEFT',
		'right' => 'RIGHT'
		];
	}

	// функция уничтожения объекта БД
	public function __destruct() {
		// закрытие соединения с БД
		$this->connection->close();
	}

	// функция загрузки конфигурационного файла
	private function load_config() {
		// чтение всего файла во временное хранилище
		$configuration = include("core/config.php");
		// получение части настроек массива БД
		$this->config = $configuration['database'];
	}

	// функция получения одной записи из БД по SQL запросу
	public function get_one($sql) {
		// вызов запроса в БД
		$result = $this->query($sql);
		// возврат преобразованного ассоциативного массива
		return $result->fetch_assoc();
	}

	// экранирование строк
	public function escapestr($str='', $like = FALSE){
		// return mysqli_real_escape_string($this->connection, $str);
		if(is_array($str)){
			foreach ($str as $key => $val){
				$str[$key] = $this->escape_str($val, $like);
			}
			return $str;
		} else {
			if($like){
				$str = str_replace(array('%', '_'), array('\\%', '\\_'), $str);
			}
			return $this->connection->real_escape_string($str);
			// if(function_exists('mysqli_real_escape_string') && isset($this->connection)){
			// 	return mysqli_real_escape_string($this->connection, $str);
			// } elseif(function_exists('mysql_escape_string')){
			// 	return mysql_escape_string($str);
			// } else {
			// 	return addslashes($str);
			// }
		}
	}

	// функция получения всех записей из БД по SQL запросу
	public function get_all($sql) {
		// вызов запроса в БД
		$result = $this->query($sql);
		// создание пустого массива для результатов
		$items = [];

		// циклическое добавление всех строк результата в созданный массив
		while ($row = $result->fetch_assoc()) {
			array_push($items, $row);
		}

		// возврат массива результатов
		return $items;
	}

	// функция запроса в БД
	public function query($sql) {
		// вызов локальной функции соединения для выполнения запроса
		$query = $this->connection->query($sql);
		// вернуть результат выполнения запроса в БД
		return $query;
	}

	// функция возврата последнего добавленного ID записи
	public function lastInsertId() {
		return $this->connection->insert_id;
	}
	
	// функция получения одной записи
	public function getItem ($assoc_query){
		$query = $this->block_select_for_item($assoc_query);
		// dbg($query);
		return $this->get_one($query);
	}

	// функция получения всех записей
	public function getItems ($assoc_query){
		$query = $this->block_select_for_item($assoc_query);
		// dbg($query);
		return $this->get_all($query);
	}
	
	// функция добавления записи
	public function addItem ($assoc_query){
		$query = $this->block_insert_for_item($assoc_query);
		// dbg($query);
		return $this->query($query);
	}

	// функция изменеия записи
	public function editItem ($assoc_query){
		$query = $this->block_update_for_item($assoc_query);
		// dbg($query);
		return $this->query($query);
	}

	private function isAssoc(array $arr) {
		if (array() === $arr) return false;
		return array_keys($arr) !== range(0, count($arr) - 1);
	}

	private function block_check_separat($key, $val){
		
		$respons = ['query'=>'','continue'=>false];

		if(preg_match('/{(?<prop>\w+)}:/', $key , $matches)){
			if($matches['prop'] == 'separat'){

				$theval = trim(strtolower($val));
				$val = ($theval == 'and' || $theval == 'or') ? strtoupper($val) : ' AND ';

				$respons['query'] = " {$val} ";
				$respons['continue'] = true;
			}
		}

		return $respons;
	}

	private function remove_last_char($string, $separator, $new_separator = ''){
		return strrev(implode(strrev($new_separator), explode(strrev(	$separator), strrev($string), 2)));
	}

	private function block_what_select_string($string_query, $the_item){
		return $this->block_what_select_arr(explode(',',$string_query), $the_item);
	}

	private function block_what_select_arr($assoc_query, $the_item){
		$query = '';
		
		if($this->isAssoc($assoc_query)){

			foreach ($assoc_query as $key => $val) {
				$key = trim($this->escapestr($key));
				$val = trim($this->escapestr($val));

				if($val != '')
					$query .= " `{$the_item}`.`{$key}` as `{$val}`, ";
				else
					$query .= " `{$the_item}`.`{$key}`, ";
			}

		} else {

			foreach ($assoc_query as $key) {
				$key = trim($this->escapestr($key));
				$query .= " `{$the_item}`.`{$key}`, ";
			}

		}

		$query = $this->remove_last_char($query, ',');

		return $query;
	}

	private function block_what_select($assoc_query, $the_item){
		$query = '';

		$the_item = $this->escapestr($the_item);

		if(isset($assoc_query)){
			
			if(is_string($assoc_query)){

				$query .= $this->block_what_select_string($assoc_query, $the_item);

			} else{

				$query .= $this->block_what_select_arr($assoc_query, $the_item);

			}

		} else {

			$query = " `{$the_item}`.* ";

		}
		return $query;
	}

	private function block_insert_qoute_field($elem, $the_item, $the_item_parent = ''){

		$query = '';

		if(preg_match('~{(?P<prop>\w+)}\.(?P<data>.+)~', $elem , $sub_matches)){
			
			$query .= " `";
			$query .= ($sub_matches['prop'] == 'up') ? $the_item_parent : $the_item;
			$query .= "`.`{$this->escapestr($sub_matches['data'])}` ";

		} else {

			$query .= " `{$the_item}`.`{$elem}` ";

		}

		return $query;
	}

	private function block_insert_qoute($elem, $default_q = '', $the_item, $the_item_parent = ''){
		
		$query = '';

		if(preg_match('/{(?<prop>\w+)}:(?<data>.+)/', $elem , $matches)){

			if($matches['prop'] == 'field'){

				$query .= $this->block_insert_qoute_field($matches['data'], $the_item, $the_item_parent);

			} else if($matches['prop'] == 'val'){

				$query .= " '{$this->escapestr($matches['data'])}' ";

			}

		} else {
			if($default_q == '`'){

				$query .= $this->block_insert_qoute_field($elem, $the_item, $the_item_parent);

			} else {
				$query .= " '{$this->escapestr($elem)}' ";
			}
		}

		return $query;
	}

	private function block_condition_for($key,$val, $the_item, $the_item_parent=''){
		$query = '';

		$query .= $this->block_insert_qoute($key, '`', $the_item, $the_item_parent);
		$query .= ' = ';
		$query .= $this->block_insert_qoute($val, '\'', $the_item, $the_item_parent);

		return $query;
	}

	private function block_condition($assoc_query, $the_item, $the_item_parent = ''){
		$query = '';

		$isassoc = $this->isAssoc($assoc_query);

		if($isassoc){
			foreach ($assoc_query as $key => $val) {

				$separat = $this->block_check_separat($key, $val);
				if($separat['query'] != '') $query .= $separat['query'];
				if($separat['continue']) continue;

				$query .= $this->block_condition_for($key,$val, $the_item, $the_item_parent);

			}
		} else {
			for($i = 0, $j = 1; $i < count($assoc_query); $i += 2, $j += 2){

				$separat = $this->block_check_separat($assoc_query[$i], $assoc_query[$j]);
				if($separat['query'] != '') $query .= $separat['query'];
				if($separat['continue']) continue;

				$query .= $this->block_condition_for($assoc_query[$i],$assoc_query[$j], $the_item, $the_item_parent);
			}
		}

		return $query;
	}

	private function block_condition_where($assoc_query, $the_item){
		$query = '';

		if(isset($assoc_query['whr'])){
			$query .= ' WHERE ';
			$query .= $this->block_condition($assoc_query['whr'], $the_item);
		}

		return $query;
	}

	private function block_condition_on($assoc_query, $the_item, $the_item_parent){
		$query = '';

		if(isset($assoc_query['on'])){
			$query .= ' ON ';
			$query .= $this->block_condition($assoc_query['on'], $the_item, $the_item_parent);
		}

		return $query;
	}

	private function block_join($assoc_query){

		$the_join = ['what' => '', 'query' => '' ];

		if( !isset($assoc_query['join']) ) return $the_join;

		$query = '';
		$query .= ' '. $this->join_type[$assoc_query['join']['type']] ." JOIN `{$assoc_query['join']['item']}` ";
		$query .= $this->block_condition_on($assoc_query['join'], $assoc_query['join']['item'], $assoc_query['item']);
		
		$the_join['what'] = ' , ' . $this->block_what_select((isset($assoc_query['join']['what']) ? $assoc_query['join']['what'] : NULL), $assoc_query['join']['item']);
		$the_join['query'] = $query;

		return $the_join;
	}

	private function block_order_by($assoc_query){

		$query = ' ORDER BY ';

		$isassoc = $this->isAssoc($assoc_query['ordby']);

		if($isassoc){
			foreach ($assoc_query['ordby'] as $key => $val) {
				$query .= " `{$assoc_query['item']}`.`{$key}` ";
				
				$theval = trim(strtolower($val));
				$val = ($theval == 'asc' || $theval == 'desc') ? strtoupper($val) : ' ASC ';

				$query .= " {$val} , ";
			}
		} else {
			for($i = 0, $j = 1; $i < count($assoc_query['ordby']); $i += 2, $j += 2){

				$query .= " `{$assoc_query['item']}`.`{$assoc_query['ordby'][$i]}` ";
				
				$theval = trim(strtolower($assoc_query['ordby'][$j]));
				$assoc_query['ordby'][$j] = ($theval == 'asc' || $theval == 'desc') ? strtoupper($assoc_query['ordby'][$j]) : ' ASC ';

				$query .= " {$assoc_query['ordby'][$j]} , ";

			}
		}
		
		$query = $this->remove_last_char($query, ',');

		return $query;
	}

	private function block_limit($assoc_query){

		$query = ' LIMIT ';

		if(is_string($assoc_query)){
			$query .= (int) $assoc_query;
		} else {
			$query .= (int) $assoc_query[0] . ' , ' . (int) $assoc_query[1];
		}
		
		return $query;
	}

	private function block_select_for_item($assoc_query){

		// tokens
		/*
			what => what select
			item => table name
			whr  => WHERE
			join  => {
				type 
				item 
				what 
				on 
			}
			ordby => ORDER BY $ $
			lim => LIMIT
		*/

		$what_select = $this->block_what_select((isset($assoc_query['what']) ? $assoc_query['what'] : NULL), $assoc_query['item']);

		$join = $this->block_join($assoc_query);

		$query = "SELECT {$what_select} {$join['what']} FROM `{$assoc_query['item']}` ";

		$query .= $join['query'];

		$query .= $this->block_condition_where($assoc_query, $assoc_query['item']);

		$query .= (isset($assoc_query['ordby'])) ? $this->block_order_by($assoc_query) : '';

		$query .= (isset($assoc_query['lim'])) ? $this->block_limit($assoc_query['lim']) : ""; 

		return $query;
	}

	private function block_insert_for_item($assoc_query){

		// tokens
		/*
			item => table name
			vals  => VALUES
		*/

		$query = "INSERT INTO `{$assoc_query['item']}` ";

		$rows = ' ( ';
		$values = ' VALUES ( ';

		foreach ($assoc_query['vals'] as $key => $val) {
			$rows .= " `$key`, ";
			$values .= " '{$this->escapestr($val)}', ";
		}

		$rows .= " ) ";
		$values .= " ) ";

		$rows = $this->remove_last_char($rows, ',');
		$values = $this->remove_last_char($values, ',');

		$query .= $rows;
		$query .= $values;

		return $query;
	}

	private function block_update_for_item($assoc_query){

		// tokens
		/*
			item => table name
			vals  => VALUES
			whr  => WHERE
			separat => for whr
		*/


		$query = "UPDATE `{$assoc_query['item']}` SET ";

		$rows = '';

		foreach ($assoc_query['vals'] as $key => $val) {
			$rows .= " `{$assoc_query['item']}`.`{$key}` = '{$this->escapestr($val)}', ";
		}

		$rows = strrev(implode(strrev(''), explode(strrev(','), strrev($rows), 2)));

		$rows .= $this->block_condition_where($assoc_query, $assoc_query['item']);

		$query .= $rows;

		return $query;
	}
}

// создание нового объекта базы данных 
// для обращения из любого места приложения
	$db = new Database();

	?>
