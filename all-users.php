<?php

	/*
	*  История поиска
	*/

	// подключение основного файла приложения
	require("core/app.php");

	$page['title'] = 'История поиска'; 
	$page['alias'] = 'all-users'; 

	$page['users'] = $db->getItems([
		'item' => 'purpose',
	]);
	$i = 0;
	foreach ($page['users'] as $item){
		$page['users'][$i]['data'] = json_decode($page['users'][$i]['data']);
		++$i;
	}

	renderPage('all-users', $page);

?>